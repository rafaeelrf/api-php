<?php

	require_once 'vendor/autoload.php';

	require_once 'src/utilities/connect-mysql.php';

	// Create and configure Slim app
	$config = ['settings' => [
	    'addContentLengthHeader' => false,
	]];
	$app = new \Slim\App($config);

	// Retorna as empresas cadastradas
	$app->get('/empresa', function ($request, $response, $args) {
		// Retorn conexão com o banco de dados MySQL
		$connection = connectDb();
		// Executa a query que retorna todas as empresas
		$stmt = $connection->prepare("SELECT * FROM empresa");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		// Converte os dados para utf-8 antes de converter o array para json
		foreach ($results as $key => $value) {
			$results[$key] = array_map("utf8_encode", $results[$key]);
		}
		// Set Header
	    $response->withHeader('Content-type', 'application/json');
	    return $response->withJson($results);
	});

	// Retorna os funcionarios da empresa
	$app->get('/empresa/{id_empresa}', function ($request, $response, $args) {
		// Get Parameters
		$id_empresa = $request->getAttribute('id_empresa');
		// Retorn conexão com o banco de dados MySQL
		$connection = connectDb();
		// Executa a query que retorna todos os funcionarios de uma empresa
		$stmt = $connection->prepare("SELECT * FROM funcionario where id_empresa = :id_empresa");
		// Bind dos parâmetros
		$stmt->bindParam(":id_empresa", $id_empresa);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		// Converte os dados para utf-8 antes de converter o array para json
		foreach ($results as $key => $value) {
			$results[$key] = array_map("utf8_encode", $results[$key]);
		}
		// Set Header
	    $response->withHeader('Content-type', 'application/json');
	    return $response->withJson($results);
	});

	// Run app
	$app->run();

